const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
    // Get Token 
    const token = req.header('x-auth-token');

    // Check If no Token 
    if (!token) {
        return res.status(401).json({ msg: 'No Token, Auth Denied' });
    }

    //Verified Token 

    try {
        const decode = jwt.verify(token, config.get('jwtSecret'));
        req.user = decode.user;
        next();
    } catch (error) {
        res.status(401).json({ msg: 'Token is not valid' });

    }
}