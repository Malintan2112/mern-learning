const express = require("express");
const request = require('request')
const config = require('config')

const router = express.Router();
const { check, validationResult } = require("express-validator/check")

const auth = require('../../middleware/auth');
const Profile = require('../../models/Profile');
const User = require('../../models/User');

// @route Get api/profile/me
// @desc Get current users profile
// @access Private 
router.get('/me', auth, async (req, res) => {

    try {
        const profile = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']);

        if (!profile) {
            return res.status(400).json({ msg: 'There is no profile for this user' });
        }
        res.json(profile);
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Server Error ')
    }
});

// @route Post api/profile
// @desc Post current users profile
// @access Private 
router.post('/', [auth, [
    check('status', 'Status is required').not().isEmpty(),
    check('skills', 'Skills  is required').not().isEmpty(),

]], async (req, res) => {
    const error = validationResult(req);
    if (!error.isEmpty()) {
        return res.status(400).json({ error: error.array() })
    }
    const {
        company,
        website,
        location,
        bio,
        status,
        githubusername,
        skills,
        youtube,
        facebook,
        twitter,
        instagram,
        linkedin
    } = req.body;
    //Build Profile object 
    const profileFields = {};
    profileFields.user = req.user.id;
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (location) profileFields.location = location;
    if (bio) profileFields.bio = bio;
    if (status) profileFields.status = status;
    if (githubusername) profileFields.githubusername = githubusername;

    // Build skills in array 

    if (skills) profileFields.skills = skills.split(',').map(skill => skill.trim());

    // Build social in object 

    profileFields.social = {}
    if (youtube) profileFields.social.youtube = youtube;
    if (facebook) profileFields.social.facebook = facebook;
    if (twitter) profileFields.social.twitter = twitter;
    if (instagram) profileFields.social.instagram = instagram;
    if (linkedin) profileFields.social.linkedin = linkedin;

    try {
        let profile = await Profile.findOne({ user: req.user.id });

        if (profile) {

            // Update
            profile = await Profile.findOneAndUpdate(
                { user: req.user.id },
                { $set: profileFields },
                { new: true }
            )
            return res.json(profile)
        }

        // Create
        profile = new Profile(profileFields);
        await profile.save()
        return res.json(profile)

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error')
    }
    res.send('Hello')





})

// @route Get api/profile/
// @desc Get all profile
// @access Public
router.get('/', async (req, res) => {
    try {
        const profiles = await Profile.find().populate('user', ['name', 'avatar']);
        res.json(profiles)
    } catch (error) {
        console.log(error.message);
        res.status(500).send('Server Error ')
    }
})

// @route Get api/profile/user/:user_id
// @desc Get profile by user_id
// @access Public
router.get('/user/:user_id', async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);
        console.log(profile)
        if (!profile) {
            return res.status(400).json({ msg: 'Profile Not Found' });
        }
        res.json(profile)
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})

// @route Delete api/profile/
// @desc Delete profile, user & post 
// @access Private
router.delete('/', auth, async (req, res) => {
    try {
        //@todo -remover user posts
        //Remove profile 
        await Profile.findOneAndRemove({ user: req.user.id });
        await User.findOneAndRemove({ _id: req.user.id });


        res.json({ msg: 'berhasil menghapus ' })
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})

// @route Create Experience  api/profile/
// @desc Create Experience profile, user & post 
// @access Private
router.put('/experience', [auth, [
    check('title', 'Title ga boleh kosong').not().isEmpty(),
    check('company', 'Company ga boleh kosong').not().isEmpty(),
    check('from', 'From ga boleh kosong').not().isEmpty(),


]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const {
        title,
        company,
        location,
        from,
        to,
        current,
        description
    } = req.body;
    const newExp = {
        title,
        company,
        location,
        from,
        to,
        current,
        description
    }

    try {
        const profile = await Profile.findOne({ user: req.user.id });
        profile.experience.unshift(newExp);
        await profile.save();

        res.json(profile)
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})

// @route Delete Experience api/profile/experience/:exp_id
// @desc Delete Experience from profile, user & post 
// @access Private
router.delete('/experience/:exp_id', auth, async (req, res) => {
    try {

        const profile = await Profile.findOne({ user: req.user.id });
        const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);
        profile.experience.splice(removeIndex, 1);

        await profile.save()
        res.json(profile)
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})

// @route Create Education  api/profile/
// @desc Create Education profile, user & post 
// @access Private
router.put('/education', [auth, [
    check('school', 'school ga boleh kosong').not().isEmpty(),
    check('degree', 'degree ga boleh kosong').not().isEmpty(),
    check('fieldofstudy', 'field of study ga boleh kosong').not().isEmpty(),
    check('from', 'From ga boleh kosong').not().isEmpty(),


]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    const {
        school,
        degree,
        fieldofstudy,
        from,
        to,
        current,
        description
    } = req.body;
    const newEdu = {
        school,
        degree,
        fieldofstudy,
        from,
        to,
        current,
        description
    }

    try {
        const profile = await Profile.findOne({ user: req.user.id });
        profile.education.unshift(newEdu);
        await profile.save();

        res.json(profile)
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})

// @route Delete Education api/profile/education/:edu_id
// @desc Delete Education from profile, user & post 
// @access Private
router.delete('/education/:edu_id', auth, async (req, res) => {
    try {

        const profile = await Profile.findOne({ user: req.user.id });
        const removeIndex = profile.education.map(item => item.id).indexOf(req.params.edu_id);
        profile.education.splice(removeIndex, 1);

        await profile.save()
        res.json(profile)
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})


// @route Create Github  api/profile/
// @desc Create Github profile, user & post 
// @access Public
router.get('/github/:username', async (req, res) => {


    try {
        const options = {
            uri: `https://api.github.com/users/${req.params.username}/repos?per_page=5&sort=created:asc&client_id=${config.get('githubClientId')}&client_secret=${config.get('githubSecret')}`,
            method: 'GET',
            headers: { 'user-agent': 'node.js' }
        }
        console.log()
        request(options, (error, response, body) => {
            if (error) console.log(error)
            if (response.statusCode !== 200) {
                return res.status(404).json({ msg: 'No Github profile found' })
            }
            return res.json(JSON.parse(body))
        })

        // res.json()
    } catch (error) {
        console.log(error.message);
        if (error.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }
})


module.exports = router; 