const express = require("express");
const router = express.Router();
const { check, validationResult } = require("express-validator/check");
const auth = require('../../middleware/auth');
const Post = require('../../models/Post');
const User = require('../../models/User');




// @route Post api/posts
// @desc Create  a post  
// @access Public
router.post('/', [
    auth, [
        check('text', 'Text is required').not().isEmpty(),
    ]
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const user = await User.findById(req.user.id).select('-password');
        const newPost = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        })
        const post = await newPost.save();
        res.json(post);
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error')
    }

})



// @route Get api/posts 
// @desc Get all post  
// @access Private
router.get('/', auth, async (req, res) => {

    try {
        const posts = await Post.find().sort({ date: -1 });
        res.json(posts);
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error')

    }

})


// @route Get api/posts/:id 
// @desc Get post by ID 
// @access Private
router.get('/:id', auth, async (req, res) => {
    try {
        const posts = await Post.findById(req.params.id);
        if (!posts) {
            return res.status(404).json({ msg: 'Post not found' });
        }
        res.json(posts);
    } catch (error) {
        console.log(error.message);
        if (error.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }

})


// @route Delete api/posts/:id 
// @desc Delete post by ID 
// @access Private
router.delete('/:id', auth, async (req, res) => {

    try {
        const post = await Post.findById(req.params.id);

        if (!post) {
            return res.status(404).json({ msg: 'Post Not Found' });
        }

        if (post.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'User  not authorized' });
        }
        await post.remove();
        return res.json({ msg: 'Delete succeess' });
    } catch (error) {
        console.log(error.message);
        if (error.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }

})


// @route PUT api/posts/like/:id 
// @desc Like a posts 
// @access Private
router.put('/like/:id', auth, async (req, res) => {

    try {
        const post = await Post.findById(req.params.id);

        if (post.likes.filter(like => like.user.toString() == req.user.id).length > 0) {
            return res.status(404).json({ msg: 'Post Already liked' });

        }
        post.likes.unshift({ user: req.user.id });
        await post.save()

        return res.json(post.likes);
    } catch (error) {
        console.log(error.message);
        if (error.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }

})
// @route PUT api/posts/unlike/:id 
// @desc unLike a posts 
// @access Private
router.put('/unlike/:id', auth, async (req, res) => {

    try {
        const post = await Post.findById(req.params.id);

        if (post.likes.filter(like => like.user.toString() == req.user.id).length === 0) {
            return res.status(404).json({ msg: 'Belum Melakukan Like' });

        }

        //Get Remove index 
        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);
        post.likes.splice(removeIndex, 1);
        await post.save()

        return res.json(post.likes);
    } catch (error) {
        console.log(error.message);
        if (error.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }

})



// @route Post api/posts/comment/:id
// @desc Comment on post 
// @access Private
router.post('/comment/:id', [
    auth, [
        check('text', 'Text is required').not().isEmpty(),
    ]
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    try {
        const user = await User.findById(req.user.id).select('-password');
        const post = await Post.findById(req.params.id);
        const newCommet = {
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        }
        post.comments.unshift(newCommet);

        await post.save()
        res.json(post.comments);
    } catch (error) {
        console.error(error.message)
        res.status(500).send('Server Error')
    }

})

// @route Delete api/posts/comment/:id/:comment_id
// @desc Delete Comment
// @access Private

router.delete('/comment/:id/:comment_id', auth, async (req, res) => {

    try {
        const post = await Post.findById(req.params.id);

        //PUll comment 
        const comment = post.comments.find(comment => comment.id === req.params.comment_id);

        //Make sure comment exist 
        if (!comment) {
            return res.status(404).json({ msg: 'Comment not found' });

        }

        // Check User
        if (comment.user.toString() != req.user.id.toString()) {
            return res.status(404).json({ msg: 'User not authorized' });

        }
        //Get Remove index 
        const removeIndex = post.comments.map(comment => comment.id.toString()).indexOf(req.params.comment_id);
        post.comments.splice(removeIndex, 1);
        await post.save();
        return res.json(post.comments);
    } catch (error) {
        console.log(error.message);
        if (error.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'id Not Found' });

        }
        res.status(500).send('Server Error ')
    }

})


module.exports = router; 