const express = require("express");
const { check, validationResult } = require("express-validator/check")
const jwt = require("jsonwebtoken");
const config = require("config")
const bcrypt = require("bcryptjs");


const auth = require("../../middleware/auth")
const User = require("../../models/User");


const router = express.Router();

// @route Get api/Auth
// @desc Test route  
// @access Public
router.get('/', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    } catch (error) {
        console.error(error.mesagge);
        res.status(500).send('Server ERror')
    }
});



// @route POST api/Auth
// @desc Login User 
// @access Public
router.post('/', [
    check('email', 'Please include valid email').isEmail(),
    check('password', 'Password is required ').exists()


], async (req, res) => {
    //console.log(req.body)
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;


    try {
        // Pengecekan user ada atu tidak 

        let user = await User.findOne({
            email
        })
        if (!user) {
            return res.status(400).json({ errors: [{ message: 'Email Not Found' }] });
        }

        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(400).json({ errors: [{ message: 'Pasword Wrong' }] });
        }
        const payload = {
            user: {
                id: user.id
            }
        }
        jwt.sign(payload, config.get('jwtSecret'), {
            expiresIn: 360000
        }, (err, token) => {
            if (err) throw err;
            res.json({ token })
        })

        // Return json web token 
        // res.send('User Registered')

    } catch (error) {

        console.log(error.message);
        res.status(500).send('Server Error');
    }

});


module.exports = router; 