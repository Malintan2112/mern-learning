const express = require("express");
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("config")
const User = require("../../models/User");
const router = express.Router();
const { check, validationResult } = require("express-validator/check")

// @route Get api/users
// @desc Test route  
// @access Public
router.post('/', [
    check('name', 'Name is required ').not().isEmpty(),
    check('email', 'Please include valid email').isEmail(),
    check('password', 'Please enter a password caracter ').isLength({
        min: 6
    }),


], async (req, res) => {
    //console.log(req.body)
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const { email, name, password } = req.body;


    try {
        // Pengecekan user ada atu tidak 

        let user = await User.findOne({
            email
        })
        if (user) {
            return res.status(400).json({ errors: [{ message: 'User Sudah ada ' }] });
        }


        // Mendapatkan icon user / mengecek gravatar

        const avatar = gravatar.url(email, {
            s: '200',
            r: 'pg',
            d: 'mm'
        })

        user = new User({
            name,
            email,
            avatar,
            password
        })

        const salt = await bcrypt.genSalt(10);

        user.password = await bcrypt.hash(password, salt);
        await user.save();


        const payload = {
            user: {
                id: user.id
            }
        }
        jwt.sign(payload, config.get('jwtSecret'), {
            expiresIn: 360000
        }, (err, token) => {
            if (err) throw err;
            res.json({ token })
        })

        // Return json web token 
        // res.send('User Registered')

    } catch (error) {

        console.log(error.message);
        res.status(500).send('Server Error');
    }

});

module.exports = router; 