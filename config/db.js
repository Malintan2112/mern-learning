const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

const connetDB = async () => {
    try {
        await mongoose.connect(db, {
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false
        });
        console.log('Mongo DB Connected ..');
    } catch (error) {
        console.log(error.message);
        // Exit processs with failure 
        process.exit(1);
    }
}

module.exports = connetDB;